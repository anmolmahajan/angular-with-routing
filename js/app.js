var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider,$locationProvider){
	$locationProvider.hashPrefix('');
	$routeProvider.when('/', {templateUrl: 'views/rootFile.html'})
	.when('/service', {templateUrl: 'views/service.html'})
	.when('/about', {templateUrl: 'views/about.html'})
	.otherwise({redirect: '/'})
})


app.factory('UserdataFactory', function($http){
	var userdata = [];
	function getUserData(){
		return $http({method : 'GET', url :'model/data.json', headers : {'Content-Type':'application/json'}})
		
		/*if(Array.isArray(userdata)){
			console.log(userdata)
			return userdata;	
		}*/
	
	//setTimeout(function(){console.log("Inside timer");return userdata},1000)	
	
	}
	return {
		getusers: getUserData
	}
})

app.directive('userDetail', function(){
	return {
		restrict: 'E', //will use user-details as element directive. Also A- Attr, M- Comment, C- Class
		template: '<h1> Template loaded for user details.</h1>'

	}
})


app.controller('userController', function($scope, UserdataFactory){
	console.log("Controller loaded");
	$scope.users = [];
	 UserdataFactory.getusers()
			.then(function(response){
				console.log(response.data.data);
				$scope.users = response.data.data;
			})
			.catch(function(err){
				console.log(err)
			});
});

app.controller('SectionController', function($scope){
	$scope.tab = 1;
	$scope.isSelected = function(tabToCheck){
		return $scope.tab == tabToCheck
	}
	$scope.selectedTab = function(seltab){
		$scope.tab = seltab;
	}
})

app.controller('FormController', function($scope){
	$scope.comment = {}
	$scope.formSubmit = function(user){
		user.comments.push($scope.comment)
		$scope.comment = {};
	}
})

app.filter('dopeFilter', function(){
	return function(str, isFemale){
		if(isFemale){
			return 'Ms. ' + str;
		}
		return 'Mr. ' + str;
	}
})